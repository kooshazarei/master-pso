package source;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.CloudletSchedulerSpaceShared;
import org.cloudbus.cloudsim.CloudletSchedulerTimeShared;
import org.cloudbus.cloudsim.Datacenter;
import org.cloudbus.cloudsim.DatacenterCharacteristics;
import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.Pe;
import org.cloudbus.cloudsim.Storage;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.VmAllocationPolicySimple;
import org.cloudbus.cloudsim.VmSchedulerTimeShared;
import org.cloudbus.cloudsim.core.CloudSim;
import org.cloudbus.cloudsim.provisioners.BwProvisionerSimple;
import org.cloudbus.cloudsim.provisioners.PeProvisionerSimple;
import org.cloudbus.cloudsim.provisioners.RamProvisionerSimple;

/**
 *
 * @author koosha zarei
 */
public class kmain {

    static FileOutputStream out;
    static PrintStream ps;
    static double averageexecutiontime;

    // main function
    public static void main(String[] args) throws IOException {
        
        // new file stream
        try {
            out = new FileOutputStream("Simulation Files\\simulation_output.txt");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        ps = new PrintStream(out);

        // print format
        DecimalFormat dft = new DecimalFormat("###.##");

        // number of running pso
        int iteration = 50;
        // calculate average time
        averageexecutiontime = 0.0;

        double[] executionlist = new double[iteration];

        // run pso for n times
        for (int i = 0; i < iteration; i++) {
            executionlist[i] = initSimulation();
            ps.print(dft.format(executionlist[i]) + " \n");
        }

        // calculate average and print mean and variance
        Statistics stat = new Statistics(executionlist);
        Log.printLine(" mean : " + stat.getMean());
        Log.printLine(" variance : " + stat.getStdDev());

        // close file
        ps.close();

    }

    // initialize simulation
    private static double initSimulation() {
        try {
            // user
            int num_user = 1;
            //get time
            Calendar calendar = Calendar.getInstance();
            boolean trace_flag = false;
            CloudSim.init(num_user, calendar, trace_flag);
            
            // create datacenter
            Datacenter datacenter1 = createDatacenter("Datacenter_1");

            // create broker
            DatacenterBroker broker = createBroker(1);
            
            // get broker id
            int brokerId = broker.getId();

            // create vms
            List<Vm> vmlist = createVMs(5, brokerId, true);

            // submit vms to broker
            broker.submitVmList(vmlist);

            // get cloudlet from dataset
            List<Cloudlet> cloudletList = createCloudLets();

            // set same userid for all cloudlets
            for (Cloudlet cloudlet : cloudletList) {
                //set all cloudlets to be managed by one broker.
                cloudlet.setUserId(brokerId);
            }

            // submit cloudlet list to the broker >>>
            broker.submitCloudletList(cloudletList);

            //----------------------------------------------------------
            //=========================================================
            // mapping !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1
//                        broker.bindCloudletToVm(brokerId, brokerId);
            // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            //==================================
            //-----------------------------------------------------------
            // start cloudsim
            CloudSim.startSimulation();
            // stop cloudsim
            CloudSim.stopSimulation();
            
            // get list of scheduled cloudlets
            List<Cloudlet> newList = broker.getCloudletReceivedList();

            // print 
            printCloudletList(newList);

            // get timing
            Cloudlet cltemp = newList.get(newList.size() - 1);
            return cltemp.getFinishTime();

        } catch (Exception e) {
            e.printStackTrace();
            Log.printLine("Unwanted errors happen");
            return 0;
        }
    }

    private static Datacenter createDatacenter(String name) {

        // Here are the steps needed to create a PowerDatacenter:
        // 1. We need to create a list to store our machine
        List<Host> hostList = new ArrayList<Host>();

        // 2. create hosts, where Every Machine contains one or more PEs or CPUs/Cores
        //((( Host 1 )))--------------------------------------------------------------------------------------------------------
        List<Pe> Host_1_peList = new ArrayList<Pe>();

        //get the mips value of the selected processor
        int Host_1_mips = Processors.Intel.Core_2_Extreme_X6800.mips;
        //get processor's number of cores
        int Host_1_cores = Processors.Intel.Core_2_Extreme_X6800.cores;

        // 3. Create PEs and add these into a list.
        for (int i = 0; i < Host_1_cores; i++) {
            //mips/cores => MIPS value is cumulative for all cores so we divide the MIPS value among all of the cores
            Host_1_peList.add(new Pe(i, new PeProvisionerSimple(Host_1_mips / Host_1_cores))); // need to store Pe id and MIPS Rating
        }

        // 4. Create Host with its id and list of PEs and add them to the list of machines
        int host_1_ID = 1;
        int host_1_ram = 2048; // host memory (MB)
        long host_1_storage = 1048576; // host storage in MBs
        int host_1_bw = 10240; // bandwidth in MB/s

        hostList.add(new Host(host_1_ID, new RamProvisionerSimple(host_1_ram),
                new BwProvisionerSimple(host_1_bw), host_1_storage, Host_1_peList,
                new VmSchedulerTimeShared(Host_1_peList)));

        //((( \Host 1 )))--------------------------------------------------------------------------------------------------------
        //((( Host 2 )))--------------------------------------------------------------------------------------------------------
        List<Pe> Host_2_peList = new ArrayList<Pe>();

        //get the mips value of the selected processor
        int Host_2_mips = Processors.Intel.Core_i7_Extreme_Edition_3960X.mips;
        //get processor's number of cores
        int Host_2_cores = Processors.Intel.Core_i7_Extreme_Edition_3960X.cores;

        // 3. Create PEs and add these into a list.
        for (int i = 0; i < Host_2_cores; i++) {
            //mips/cores => MIPS value is cumulative for all cores so we divide the MIPS value among all of the cores
            Host_2_peList.add(new Pe(i, new PeProvisionerSimple(Host_2_mips / Host_2_cores))); // need to store Pe id and MIPS Rating
        }

        // 4. Create Host with its id and list of PEs and add them to the list of machines
        int host_2_id = 2;
        int host_2_ram = 2048; // host memory (MB)
        long host_2_storage = 1048576; // host storage in MBs
        int host_2_bw = 10240; // bandwidth in MB/s

        hostList.add(new Host(host_2_id, new RamProvisionerSimple(host_2_ram),
                new BwProvisionerSimple(host_2_bw), host_2_storage, Host_2_peList,
                new VmSchedulerTimeShared(Host_2_peList)));

        //((( \Host 2 )))--------------------------------------------------------------------------------------------------------
        // 5. Create a DatacenterCharacteristics object that stores the
        // properties of a data center: architecture, OS, list of
        // Machines, allocation policy: time- or space-shared, time zone
        // and its price (G$/Pe time unit).
        String arch = "x86"; // system architecture
        String os = "Linux"; // operating system
        String vmm = "Xen";
        double time_zone = 10.0; // time zone this resource located
        double cost = 3.0; // the cost of using processing in this resource
        double costPerMem = 0.05; // the cost of using memory in this resource
        double costPerStorage = 0.001; // the cost of using storage in this resource
        double costPerBw = 0.0; // the cost of using bw in this resource
        LinkedList<Storage> storageList = new LinkedList<Storage>(); // we are not adding SAN devices by now

        DatacenterCharacteristics characteristics = new DatacenterCharacteristics(
                arch, os, vmm, hostList, time_zone, cost, costPerMem,
                costPerStorage, costPerBw);

        // 6. Finally, we need to create a PowerDatacenter object.
        Datacenter datacenter = null;
        try {
            datacenter = new Datacenter(name, characteristics,
                    new VmAllocationPolicySimple(hostList), storageList, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return datacenter;
    }

    private static DatacenterBroker createBroker(int id) {
        DatacenterBroker broker = null;
        try {
            broker = new DatacenterBroker("Broker" + id);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return broker;
    }

    private static List<Vm> createVMs(int VMNr, int brokerId, boolean timeSharedScheduling) {

        List<Vm> vmlist = new ArrayList<Vm>();

        // VM description
        int mips = Processors.Intel.Pentium_4_Extreme_Edition.mips;
        long size = 10240; // image size (MB)
        int ram = 512; // vm memory (MB)
        long bw = 1024; // MB/s
        int pesNumber = 1; // number of cpus
        String vmm = "Xen"; // VMM name

        for (int i = 0; i < VMNr; i++) {

            Vm vm;

            if (timeSharedScheduling) {
                //create VM that uses time shared scheduling to schedule Cloudlets
                vm = new Vm(i, brokerId, mips, pesNumber, ram, bw, size, vmm, new CloudletSchedulerTimeShared());
            } else {
                //create VM that uses space shared scheduling to schedule Cloudlets
                vm = new Vm(i, brokerId, mips, pesNumber, ram, bw, size, vmm, new CloudletSchedulerSpaceShared());
            }

            // add the VM to the vmList
            vmlist.add(vm);
        }

        return vmlist;
    }

    private static List<Cloudlet> createCloudLets() throws FileNotFoundException {
        // create list
        List<Cloudlet> cloudletList;

        // read from file
//		WorkloadFileReader workloadFileReader = new WorkloadFileReader("Simulation Files/HPC2N-2002-2.1-cln.swf", 1);
        MontageFileReader workloadFileReader = new MontageFileReader("Simulation Files/Montage_25.xml", 1);
//                MontageFileReader workloadFileReader = new MontageFileReader("Simulation Files/Montage_50.xml", 1);
//                MontageFileReader workloadFileReader = new MontageFileReader("Simulation Files/Montage_100.xml", 1);
//                MontageFileReader workloadFileReader = new MontageFileReader("Simulation Files/Montage_200.xml", 1);

        cloudletList = workloadFileReader.generateWorkload();
        return cloudletList;
    }

    @SuppressWarnings("unused")
    private static ArrayList<Integer> getUsersIDs(List<Cloudlet> cloudletList) {

        ArrayList<Integer> usersIDs = new ArrayList<Integer>();
        ArrayList<Integer> usersLists = new ArrayList<Integer>();

        for (Cloudlet cloudlet : cloudletList) {
            usersLists.add(cloudlet.getUserId());
        }

        HashSet<Integer> uniqueValues = new HashSet<Integer>(usersLists);

        for (int value : uniqueValues) {
            usersIDs.add(value);
        }

        return usersIDs;
    }

    private static void printCloudletList(List<Cloudlet> list) throws IOException {
        int size = list.size();
        Cloudlet cloudlet;
    }
}
