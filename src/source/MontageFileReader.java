package source;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.zip.GZIPInputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.UtilizationModel;
import org.cloudbus.cloudsim.UtilizationModelFull;
import org.cloudbus.cloudsim.util.WorkloadModel;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.File;

/**
 *
 * @author koosha zarei
 */
public class MontageFileReader implements WorkloadModel {

    private final File file; // file name

    private final int rating; // a PE rating

    private ArrayList<Cloudlet> jobs = null; // a list for getting all the

    private int JOB_NUM = 1 - 1; // job number

    private int SUBMIT_TIME = 2 - 1; // submit time of a Gridlet

    private final int RUN_TIME = 4 - 1; // running time of a Gridlet

    private final int NUM_PROC = 5 - 1; // number of processors needed for a

    // Gridlet
    private int REQ_NUM_PROC = 8 - 1; // required number of processors

    private int REQ_RUN_TIME = 9 - 1; // required running time

    private final int USER_ID = 12 - 1; // if of user who submitted the job

    private final int GROUP_ID = 13 - 1; // if of group of the user who

    // submitted the job
    private int MAX_FIELD = 18; // max number of field in the trace file

    private String COMMENT = ";"; // a string that denotes the start of a

    // comment
    private static final int IRRELEVANT = -1; // irrelevant number

    private String[] fieldArray = null; // a temp array storing all the fields

    public MontageFileReader(final String fileName, final int rating) throws FileNotFoundException {

        if (fileName == null || fileName.length() == 0) {
            throw new IllegalArgumentException("Invalid trace file name.");
        } else if (rating <= 0) {
            throw new IllegalArgumentException("Resource PE rating must be > 0.");
        }

        file = new File(fileName);
        if (!file.exists()) {
            throw new FileNotFoundException("Workload trace " + fileName + " does not exist");
        }

        this.rating = rating;
    }

    @Override
    public ArrayList<Cloudlet> generateWorkload() {
        if (jobs == null) {
            jobs = new ArrayList<Cloudlet>();

            // create a temp array
            fieldArray = new String[MAX_FIELD];

            try {

                readFile(file);

            } catch (final FileNotFoundException e) {
            } catch (final IOException e) {
            }
        }

        return jobs;
    }

    public boolean setComment(final String cmt) {
        boolean success = false;
        if (cmt != null && cmt.length() > 0) {
            COMMENT = cmt;
            success = true;
        }
        return success;
    }

    public boolean setField(
            final int maxField,
            final int jobNum,
            final int submitTime,
            final int runTime,
            final int numProc) {
        // need to subtract by 1 since array starts at 0.
        if (jobNum > 0) {
            JOB_NUM = jobNum - 1;
        } else if (jobNum == 0) {
            throw new IllegalArgumentException("Invalid job number field.");
        } else {
            JOB_NUM = -1;
        }

        // get the max. number of field
        if (maxField > 0) {
            MAX_FIELD = maxField;
        } else {
            throw new IllegalArgumentException("Invalid max. number of field.");
        }

        // get the submit time field
        if (submitTime > 0) {
            SUBMIT_TIME = submitTime - 1;
        } else {
            throw new IllegalArgumentException("Invalid submit time field.");
        }

        // get the run time field
        if (runTime > 0) {
            REQ_RUN_TIME = runTime - 1;
        } else {
            throw new IllegalArgumentException("Invalid run time field.");
        }

        // get the number of processors field
        if (numProc > 0) {
            REQ_NUM_PROC = numProc - 1;
        } else {
            throw new IllegalArgumentException("Invalid number of processors field.");
        }

        return true;
    }

    private void createJob(final int id, final long submitTime, final int runTime,
            final int numProc, final int reqRunTime, final int userID, final int groupID) {

        // length (execution time of a job) = run time in second * how many instruction executed per second
        // for example 1000 seconds * 1 per second = 1000 instructions the length of the job
        final int len = runTime * rating;

        UtilizationModel utilizationModel = new UtilizationModelFull();

        final Cloudlet wgl = new Cloudlet(id, len, numProc, 0, 0, utilizationModel,
                utilizationModel, utilizationModel);

        wgl.setUserId(userID);

        jobs.add(wgl);

        /*
		  	int id = 4; //Job number [1]
			int len = runTime * rating; // Run Time [4]* 1 ... (execution time)
			int numProc = 2; //requested number of processors [8]
			UtilizationModel utilizationModel = new UtilizationModelFull();
		
			Cloudlet(id, len, numProc,	0, 0, utilizationModel, utilizationModel, utilizationModel);
			
			(vs.)
			
			int id = 0;
			pesNumber=1;
			long length = 250000;// in million instruction
			long fileSize = 300;
			long outputSize = 300;
			UtilizationModel utilizationModel = new UtilizationModelFull();
	
			Cloudlet(id, length, pesNumber, fileSize, outputSize, utilizationModel, utilizationModel, utilizationModel);
					 
         */
    }

    private void extractField(final String[] array, final int line) {
        try {
            Integer obj = null;

            // get the job number
            int id = 0;
            if (JOB_NUM == IRRELEVANT) {
                id = jobs.size() + 1;
            } else {
                obj = new Integer(array[JOB_NUM].trim());
                id = obj.intValue();
            }

            // get the submit time
            final Long l = new Long(array[SUBMIT_TIME].trim());
            final long submitTime = l.intValue();

            // get the user estimated run time
            obj = new Integer(array[REQ_RUN_TIME].trim());
            final int reqRunTime = obj.intValue();

            // if the required run time field is ignored, then use
            // the actual run time
            obj = new Integer(array[RUN_TIME].trim());
            int runTime = obj.intValue();

            final int userID = new Integer(array[USER_ID].trim()).intValue();
            final int groupID = new Integer(array[GROUP_ID].trim()).intValue();

            // according to the SWF manual, runtime of 0 is possible due
            // to rounding down. E.g. runtime is 0.4 seconds -> runtime = 0
            if (runTime <= 0) {
                runTime = 1; // change to 1 second
            }

            // get the number of allocated processors
            obj = new Integer(array[REQ_NUM_PROC].trim());
            int numProc = obj.intValue();

            // if the required num of allocated processors field is ignored
            // or zero, then use the actual field
            if (numProc == IRRELEVANT || numProc == 0) {
                obj = new Integer(array[NUM_PROC].trim());
                numProc = obj.intValue();
            }

            // finally, check if the num of PEs required is valid or not
            if (numProc <= 0) {
                numProc = 1;
            }
            createJob(id, submitTime, runTime, numProc, reqRunTime, userID, groupID);
        } catch (final Exception e) {

        }
    }

    private boolean readFile(final File fl) throws IOException, FileNotFoundException {
        boolean success = false;

        try {

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fl);

            doc.getDocumentElement().normalize();

            System.out.println("Root element :" + doc.getDocumentElement().getNodeName());

            NodeList nList = doc.getElementsByTagName("job");
            System.out.println("----------------------------");

            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
                //System.out.println("Current Element :" + nNode.getNodeName());

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;

                    float rt = new Float(eElement.getAttribute("runtime")).floatValue();
                    int runTime = (int) (rt * 1000);
                    int numProc = 30;

                    createJob(temp, 0, runTime, numProc, 0, 0, 0);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return success;
    }

}
